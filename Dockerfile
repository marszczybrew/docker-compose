FROM tmaier/docker-compose:18

RUN apk add --no-cache py-pip openssl bash make
RUN pip install docker-compose